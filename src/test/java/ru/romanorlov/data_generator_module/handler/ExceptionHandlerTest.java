package ru.romanorlov.data_generator_module.handler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.romanorlov.data_generator_module.exception.CannotGetTitlesListException;
import ru.romanorlov.data_generator_module.exception.NoSuchCurrencyPairException;
import ru.romanorlov.data_generator_module.exception.handler.GlobalGeneratorExceptionHandler;

@ExtendWith(MockitoExtension.class)
public class ExceptionHandlerTest {
  private final String MESSAGE = "TEST";

  @InjectMocks
  private GlobalGeneratorExceptionHandler handler;

  @Test
  @DisplayName("Checking for catching NoSuchCurrencyPairException")
  void testHandleException_NoSuchCurrencyPairException() {
    ResponseEntity<String> expected = new ResponseEntity<>(MESSAGE, HttpStatus.NOT_FOUND);

    ResponseEntity<String> actual = handler.handleException(new NoSuchCurrencyPairException(MESSAGE));

    assertNotNull(actual);
    assertEquals(expected, actual);
  }

  @Test
  @DisplayName("Checking for catching CannotGetTitlesListException")
  void testHandleException_CannotGetTitlesListException() {
    ResponseEntity<String> expected = new ResponseEntity<>(MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);

    ResponseEntity<String> actual = handler.handleException(new CannotGetTitlesListException(MESSAGE));

    assertNotNull(actual);
    assertEquals(expected, actual);
  }

  @Test
  @DisplayName("Checking for catching RuntimeException")
  void testHandleException_RuntimeException() {
    ResponseEntity<String> expected = new ResponseEntity<>(MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);

    ResponseEntity<String> actual = handler.handleException(new RuntimeException(MESSAGE));

    assertNotNull(actual);
    assertEquals(expected, actual);
  }
}
