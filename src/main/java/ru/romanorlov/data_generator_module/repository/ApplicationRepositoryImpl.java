package ru.romanorlov.data_generator_module.repository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import ru.romanorlov.data_generator_module.config.ApplicationConfig;
import ru.romanorlov.data_generator_module.exception.NoSuchCurrencyPairException;

@Repository
@Getter
public class ApplicationRepositoryImpl implements ApplicationRepository {
  private final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("MMMM-dd-yyyy HH:mm:ss");
  private final ApplicationConfig config;
  private final Map<String, Deque<PriceValue>> DATA;
  private final String TIME_ZONE;

  @Autowired
  public ApplicationRepositoryImpl(ApplicationConfig config) {
    this.config = config;
    TIME_ZONE = config.getTimeZone();
    DATA = new HashMap<>();

    config.getPairs().forEach(title -> DATA.put(title, new LinkedList<>()));
  }

  public List<String> getCurrencyPairsList() {
    return config.getPairs();
  }

  public Deque<PriceValue> getCurrencyPairPriceHistory(String title) {
    Deque<PriceValue> priceHistory = DATA.get(title);

    if (priceHistory == null) {
      throw new NoSuchCurrencyPairException(
          "There is no currency pair with title = " + title);
    };

    return priceHistory;
  }

  @Scheduled(fixedDelay = 1000)
  private void updateInfoAboutPrice() {
    ExecutorService threadPool = Executors.newFixedThreadPool(4);

    DATA.keySet().forEach(key -> {
      threadPool.execute(() -> {
        addNewValue(key);
      });
    });
  }

  private void addNewValue(String pairTitle) {
    LocalDateTime now = ZonedDateTime.now(ZoneId.of(TIME_ZONE)).toLocalDateTime();

    double resultTimeFunction = LocalTime.now().getSecond() % 2 == 0 ? 1.1 : 0.9;
    double aLittleBitRandom = Math.random() - 0.5;
    double constant = pairTitle.hashCode() % 10;

    if (constant < 0) constant *= -1;

    Deque<PriceValue> priceHistory = DATA.get(pairTitle);

    if (priceHistory.size() == 100) {
      priceHistory.pollLast();
    }

    priceHistory.addFirst(new PriceValue(constant * aLittleBitRandom * resultTimeFunction, DATE_PATTERN.format(now)));
  }

  @Data
  @AllArgsConstructor
  public static class PriceValue {
    private double price;
    private String date;
  }
}
