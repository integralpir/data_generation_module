package ru.romanorlov.data_generator_module.controller;

import java.util.Deque;
import java.util.LinkedList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.romanorlov.data_generator_module.exception.CannotGetTitlesListException;
import ru.romanorlov.data_generator_module.repository.ApplicationRepositoryImpl.PriceValue;
import ru.romanorlov.data_generator_module.service.ApplicationService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ApplicationControllerTest {
    private final static String TITLE = "TEST1";

    @Mock
    private ApplicationService service;

    @InjectMocks
    private ApplicationController controller;


    @Test
    @DisplayName("Получение информации по конкретной валютной паре.")
    void testGetCurrencyPairByTitle() {
        final Deque<PriceValue> deque = new LinkedList<>();
        when(service.getCurrencyPairPriceHistory(TITLE)).thenReturn(deque);

        final Deque<PriceValue> actual = controller.getPriceHistoryCurrencyPairByTitle(TITLE);

        assertNotNull(actual);
        assertEquals(deque, actual);
        verify(service).getCurrencyPairPriceHistory(TITLE);
    }

    @Test
    @DisplayName("Получение списка доступных валютных пар.")
    void testGetAvailablePairs() {
        final List<String> emptyList = new ArrayList<>();
        when(service.getAvailablePairs()).thenReturn(emptyList);

        assertThrows(CannotGetTitlesListException.class,
                () -> controller.getAvailablePairs());

        final List<String> fullyList = new ArrayList<>(){{
            add(TITLE);
        }};
        when(service.getAvailablePairs()).thenReturn(fullyList);

        final List<String> actual = controller.getAvailablePairs();

        assertNotNull(actual);
        assertEquals(fullyList, actual);
        verify(service, Mockito.times(2)).getAvailablePairs();
    }
}
