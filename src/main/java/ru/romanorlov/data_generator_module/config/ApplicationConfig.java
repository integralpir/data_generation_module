package ru.romanorlov.data_generator_module.config;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "config")
@Data
public class ApplicationConfig {
  private List<String> pairs;
  private String timeZone;
}
