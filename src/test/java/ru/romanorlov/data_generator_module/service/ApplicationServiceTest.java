package ru.romanorlov.data_generator_module.service;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.romanorlov.data_generator_module.repository.ApplicationRepository;
import ru.romanorlov.data_generator_module.repository.ApplicationRepositoryImpl.PriceValue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ApplicationServiceTest {
    private static final String TITLE = "TEST";

    @Mock
    private ApplicationRepository repository;

    @InjectMocks
    private ApplicationServiceImpl service;

    @Test
    @DisplayName("Получение информации по валютной паре.")
    void testGetCurrencyPairPriceHistory() {
        Deque<PriceValue> expected = new LinkedList<>();
        when(repository.getCurrencyPairPriceHistory(TITLE)).thenReturn(expected);

        Deque<PriceValue> actual = service.getCurrencyPairPriceHistory(TITLE);

        assertNotNull(actual);
        assertEquals(expected, actual);
        verify(repository).getCurrencyPairPriceHistory(TITLE);
    }

    @Test
    @DisplayName("Получение списка доступных пар.")
    void testGetAvailablePairs() {
      List<String> expected = new ArrayList<>();
      when(repository.getCurrencyPairsList()).thenReturn(expected);

      List<String> actual = service.getAvailablePairs();

      assertNotNull(actual);
      assertEquals(expected, actual);
      verify(repository).getCurrencyPairsList();
    }
}