package ru.romanorlov.data_generator_module.service;

import java.util.Deque;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import ru.romanorlov.data_generator_module.repository.ApplicationRepository;
import ru.romanorlov.data_generator_module.repository.ApplicationRepositoryImpl.PriceValue;


@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService{
    private final ApplicationRepository repository;

    public List<String> getAvailablePairs() {
        return repository.getCurrencyPairsList();
    }

    public Deque<PriceValue> getCurrencyPairPriceHistory(String title) {
        return repository.getCurrencyPairPriceHistory(title);
    }
}
