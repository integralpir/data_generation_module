package ru.romanorlov.data_generator_module.aspect;


import java.util.Deque;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import ru.romanorlov.data_generator_module.config.ApplicationConfig;
import ru.romanorlov.data_generator_module.repository.ApplicationRepositoryImpl.PriceValue;

@Component
@Aspect
@Slf4j
@RequiredArgsConstructor
public class ApplicationAspect {
    private final ApplicationConfig config;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Pointcut("execution(* ru.romanorlov.data_generator_module.controller.ApplicationController.getPriceHistoryCurrencyPairByTitle(..))")
    public void getPriceHistoryCurrencyPairByTitlePointCut() {}

    @Pointcut("execution(* ru.romanorlov.data_generator_module.controller.ApplicationController.getAvailablePairs(..))")
    public void getAvailablePairsPointCut() {}

    @AfterReturning(pointcut = "getPriceHistoryCurrencyPairByTitlePointCut()",
                    returning = "priceHistory")
    public void afterGetCurrencyPairByTitleReturningAdvice(JoinPoint joinPoint, Deque<PriceValue> priceHistory) {
        log.info("The controller gave the information about currency pair with title: "
                + joinPoint.getArgs()[0] + ": "
                + ZonedDateTime.now(ZoneId.of(config.getTimeZone())).format(FORMATTER));
    }

    @AfterReturning(pointcut = "getAvailablePairsPointCut()",
                    returning = "availablePairs")
    public void afterGetAvailablePairsReturningAdvice(JoinPoint joinPoint, List<String> availablePairs) {
        log.info("The controller gave the information about available titles: "
                + ZonedDateTime.now(ZoneId.of(config.getTimeZone())).format(FORMATTER));
    }

    @AfterThrowing(pointcut = "getAvailablePairsPointCut()",
            throwing = "exception")
    public void afterGetAvailablePairsThrowingAdvice(Throwable exception) {
        log.error("The configuration file does not contain a list of available currency pairs!!! "
                + exception.getMessage() + " "
                + ZonedDateTime.now(ZoneId.of(config.getTimeZone())).format(FORMATTER));
    }
}
