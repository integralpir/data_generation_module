package ru.romanorlov.data_generator_module.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.romanorlov.data_generator_module.exception.CannotGetTitlesListException;
import ru.romanorlov.data_generator_module.exception.NoSuchCurrencyPairException;

@RestControllerAdvice
public class GlobalGeneratorExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<String> handleException(NoSuchCurrencyPairException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleException(CannotGetTitlesListException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleException(RuntimeException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
