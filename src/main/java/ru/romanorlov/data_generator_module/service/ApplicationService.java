package ru.romanorlov.data_generator_module.service;

import java.util.Deque;
import java.util.List;
import ru.romanorlov.data_generator_module.repository.ApplicationRepositoryImpl.PriceValue;

public interface ApplicationService {
    List<String> getAvailablePairs();
    Deque<PriceValue> getCurrencyPairPriceHistory(String title);
}
