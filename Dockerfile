FROM adoptopenjdk/openjdk11 as builder

WORKDIR /src
ADD . .
RUN ./mvnw clean package

FROM adoptopenjdk/openjdk11 as runner

EXPOSE 8081
WORKDIR /src
COPY --from=builder src/target/data_generator_module-0.0.1-SNAPSHOT.jar .
COPY --from=builder src/logs .

CMD [ "java", "-jar", "data_generator_module-0.0.1-SNAPSHOT.jar"]