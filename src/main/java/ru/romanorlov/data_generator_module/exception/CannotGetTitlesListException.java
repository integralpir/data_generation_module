package ru.romanorlov.data_generator_module.exception;

public class CannotGetTitlesListException extends RuntimeException {
    public CannotGetTitlesListException(String message) {
        super(message);
    }
}
