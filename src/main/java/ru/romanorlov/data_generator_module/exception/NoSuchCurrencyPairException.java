package ru.romanorlov.data_generator_module.exception;


public class NoSuchCurrencyPairException extends RuntimeException {
    public NoSuchCurrencyPairException(String message) {
        super(message);
    }
}
