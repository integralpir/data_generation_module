package ru.romanorlov.data_generator_module.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Deque;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.romanorlov.data_generator_module.exception.CannotGetTitlesListException;
import ru.romanorlov.data_generator_module.repository.ApplicationRepositoryImpl.PriceValue;
import ru.romanorlov.data_generator_module.service.ApplicationService;

import java.util.List;

@RestController
@RequestMapping(value = "/pair")
@RequiredArgsConstructor
@Tag(
    name = "Контроллер",
    description = "Все доступные эндпоинты."
)
public class ApplicationController {
    private final ApplicationService service;

    @GetMapping(value = "/{pairTitle}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Получение информации о конкретной валютной паре.")
    public Deque<PriceValue> getPriceHistoryCurrencyPairByTitle(
        @Parameter(description = "Название валютной пары.")
        @PathVariable String pairTitle) {
        return service.getCurrencyPairPriceHistory(pairTitle);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Получение поддерживаемых валютных пар.")
    public List<String> getAvailablePairs() {
        List<String> pairs = service.getAvailablePairs();

        if (pairs.isEmpty()) {
            throw new CannotGetTitlesListException(
                "Sorry, for technical reasons we can't get a list of current currency pairs");
        }

        return pairs;
    }
}
