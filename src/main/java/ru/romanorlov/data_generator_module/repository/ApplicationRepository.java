package ru.romanorlov.data_generator_module.repository;

import java.util.Deque;
import java.util.List;

public interface ApplicationRepository {
  List<String> getCurrencyPairsList();
  Deque<ApplicationRepositoryImpl.PriceValue> getCurrencyPairPriceHistory(String title);
}
